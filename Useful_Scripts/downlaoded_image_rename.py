import re
import os

with os.scandir(os.getcwd()) as files:
   for file in files:
      new_name = re.sub(r'-\d+', '', file.name)
      os.rename(file.name, new_name)