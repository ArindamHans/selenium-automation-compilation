'''
⚠ Problem : I need to change the timings in a subtitle file so that the subtitles can be 				in-sync with the video.

This script reads the contents of .srt file and changes them
'''

from os.path import expanduser
import re
from datetime import datetime, timedelta

def increment(matchObj):
	time_str = matchObj[0]
	time_obj = (datetime.strptime(time_str, '%H:%M:%S,%f'))
	return (((time_obj - timedelta(seconds=27)).time()).strftime('%H:%M:%S,%f')[:-3])

with open(expanduser("~/Videos/MOVIES/A Silent Voice (2016)/subtitles/english.srt")) as infile:
	lines = re.sub(r'\d\d:\d\d:\d\d,\d\d\d', increment, infile.read())
	with open(expanduser("~/Videos/MOVIES/A Silent Voice (2016)/subtitles/mod_english.srt"), 'w') as outfile:
		outfile.write(lines)



