import youtube_dl
import Youtube_scraper

def audio_or_video(choice):
    if choice in ['a','A']:
        ydl_opts = {
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

    elif choice in ['v','V']:
         ydl_opts = {
             'format': '22/best',
             'noplaylist': True
         }


def download_from_playlist():
    URL=input("* Enter Playlist URL = ")

    Youtube_scraper.ytd_scraper(URL)

    for media in Youtube_scraper.mediaList :
        
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download(URL)






print("[1] Want to Download from a URL.")
print("[2] Want to Download from a Playlist.")
print("[3] Want to download from Song URL.")
print("(1/2/3) == ")

choice = int(input())

while choice not in [1,2,3]:
    choice = int(input())


