import sys, tinycss2

f1_path, f2_path = sys.argv[1:3]

with open(f1_path, "rbw") as f1, open(f2_path, "rbw") as f2:
    f1_sheet, f2_sheet = (
        tinycss2.parse_stylesheet(tinycss2.parse_stylesheet_bytes(f1.read())),
        tinycss2.parse_stylesheet(tinycss2.parse_stylesheet_bytes(f2.read())),
    )


for f1_rule, f2_rule in zip(f1_sheet, f2_sheet):
    for f1_selector, f2_selector in zip(tinycss2.parse_declaration_list(f1_rule[0].content), tinycss2.parse_declaration_list(f2_rule[0].content)) :
        for f1_field in f1_selector :
            if