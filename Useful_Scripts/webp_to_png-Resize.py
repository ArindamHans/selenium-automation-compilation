import os
import sys
from shutil import copyfile
import subprocess

root = sys.argv[1]
new_root = sys.argv[2]
max_img_width = sys.argv[3]

dirs = os.scandir(root)
os.mkdir(new_root)

for dir in dirs:
   files = os.scandir(root+'/'+dir.name)
   os.mkdir(new_root+'/'+dir.name)

   for img in files:
      img_path = root+'/'+dir.name+'/'+img.name
      new_img_path = new_root+'/'+dir.name+'/'+img.name
      try:
         img_width = subprocess.run(['identify', '-format', '\'%w\'', img_path], stdout=subprocess.PIPE)
         img_width = int(str(img_width.stdout, 'utf-8')[1:-1])
         if(img_width > max_img_width ):
            img_width = 400
         if img.name[-5:] == '.webp':
            new_img_path = new_img_path[:-5]+'.png'
            subprocess.run(['ffmpeg', '-i', img_path, '-vf', f'scale=-1:{img_width}', new_img_path], check=True)
         else:
            subprocess.run(['ffmpeg', '-i', img_path, '-vf', f'scale=-1:{img_width}', new_img_path], check=True)
      except:
         try:
            copyfile(img_path, new_img_path)
         except:
            pass



