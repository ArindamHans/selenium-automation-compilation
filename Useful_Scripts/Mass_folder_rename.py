# The video thumbnails were not visible in NAUTILUS. 
# I applied some of the solutions given on the Internet to my problutem.
# I was unable to solve it.
# I only had to change the file name a little bit.
# So the program just change the file name a little bit ( by adding a space to the end )..

import os
import re
import sys

def change_name_a_bit(file_path, regex_pattern, repl):
    print(f'file_path: {file_path} || regex: {regex_pattern} || repl: {repl}')
    os.chdir(file_path)
    with os.scandir(file_path) as files :
        for file_ in files:
            new_name = re.sub(rf'{regex_pattern}', repl, file_.name)
            print('current : ', file_.name, ' || New_name : ', new_name)
            os.rename(file_.name, new_name)
        print("--TASK COMPLETED--")        


if __name__ == "__main__":
    change_name_a_bit(sys.argv[1], sys.argv[2], sys.argv[3])


# file_path = input("Enter the path = ")  

# x = input("Do you want to proceed ?? (Y/y | N/n) = ").lower()

# if(x == 'y'):
#     change_name_a_bit(file_path)

# else:
#     os._exit(0)
