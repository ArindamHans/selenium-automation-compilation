from bs4 import BeautifulSoup as bs

import requests



def ytd_playlist_scraper(url):

        page_response = requests.get(url, timeout=5)

        page_content = bs(page_response.content, "html.parser")

        medias = page_content.find_all('a')
 
        mediaList = []
        mediaDict = {}

        print("Function entered !!")
        for media in medias:

                link = 'https://www.youtube.com' + str(media['href'])
                for media_info in (page_content.findChild('span')) :
                        mediaDict = {'title': str(media_info['title']), 'aria-label':str(media_info['aria-label']), 'link':str(link)}
                        mediaList.append(mediaDict)
#                        print(mediaList,end="/n")
        return(mediaList)


                

def ytd_search_scraper(search):
        base = "https://www.youtube.com/results?search_query="
        search = '+'.join(search.split(' '))

        page_response = requests.get(base+search, timeout=5)

        page_content = bs(page_response.content, "html.parser")

        medias = page_content.find_all('a',class_="yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink spf-link ")

        mediaList = []
        mediaDict = {}

        print("Function entered !!")
        for media in medias:
                mediaDict = {'title': str(media['title']), 'link':str('https://www.youtube.com' + media['href'])}
                mediaList.append(mediaDict)
#                        print(mediaList,end="/n")
        return(mediaList)




#print(ytd_search_scraper("hello adele"))
