from splinter import Browser
import os
import shutil
import sys
import subprocess
from guesslang import Guess
import requests
import atexit

browser = Browser('chrome')
guess = Guess()

CLIENT_SECRET = ''
dir_depth = 976
log_file = open('hack.log', 'a')
zip_dir = key = code = lang = found = None
# os.chdir('coding_300')


def main(ini_path='/home/arion/CODE/CTF/coding_300/'):
    os.chdir(ini_path)

    while True:
        global key, code, lang, dir_depth, found
        found = False
        key = code = lang = None

        print('\n[ {} ]'.format(dir_depth))
        log_file.write('\n[ {} ]'.format(dir_depth))

        decode_qr()

        if not key:
            try:
                lang = guess.language_name(code).upper()
            except Exception as e:
                print("[KEY_ENTER_ERROR]  {}\n\n".format(e))
                lang = input('\n🔤 Enter LANG manually : ')
            if lang == 'PYTHON':
                lang = 'PYTHON3'
            if lang == 'BATCHFILE' or lang == 'C#' or lang == 'COFFEESCRIPT' or lang == 'JAVASCRIPT' or lang == 'TYPESCRIPT' or lang == 'SCALA' or lang == 'RUBY':
                lang = 'JAVASCRIPT_NODE'
            if lang == 'POWERSHELL':
                lang = 'PHP'

            print('\nLanguage : {}\n'.format(lang))

            if (lang == 'SHELL'):
                shell_script = open(
                    '/home/arion/CODE/CTF/shell_script.sh', 'w')
                shell_script.write(code)
                shell_script.close()
                key = subprocess.run(
                    ['bash', '/home/arion/CODE/CTF/shell_script.sh'], stdout=subprocess.PIPE)
                key = key.stdout.decode()[:-1]

            else:
                run_code()

            print('\n🔑 KEY : {}\n'.format(key))

        extract_zip()

        dir_depth += 1
        print("\n-------------------------------------------------------------------\n")
        log_file.write(
            "\n-------------------------------------------------------------------\n")

        if found == False:
            break

    log_file.close()


def extract_zip():
    global zip_dir, found, log_file, key
    for item in os.listdir():
        if item[-4:] == ".zip":
            print("Extracting {} ....".format(item))
            try:
                zip_dir = item[:-4]
                extract = subprocess.run(
                    ["7z", "x", item, '-p{}'.format(key), '-y'], stdout=subprocess.PIPE)
                if 'Everything is Ok' in str(extract):
                    os.remove(zip_dir + '.zip')
                    os.remove(zip_dir + '.png')
                    print('Current Directory : {}'.format(os.getcwd()))
                    found = True
                    log_file.write('[7Zip]  {}\n\n'.format(
                        extract.stdout.decode()))
                    print("✅ Extraction Done!\n")
                else:
                    raise Exception('\nEverything is not Ok')
            except Exception as e:
                print("⚠️ Zip Extraction Failed ⚠️\n")
                log_file.write("[EXTRACT_ZIP_ERROR]  {}\n\n".format(e))
                try:
                    key = input('\n🔑 Enter KEY manually 🔑 : ')
                    extract_zip()
                except Exception as e:
                    print("[KEY_ENTER_ERROR]  {}\n\n".format(e))


def decode_qr():
    global log_file, code, key
    for item in os.listdir():
        if item[-4:] == ".png":
            print("Decoding {} ....".format(item))
            try:
                print('Current Directory : {}'.format(os.getcwd()))
                browser.visit('https://zxing.org/w/decode.jspx')
                qr = browser.attach_file('f', os.path.join(os.getcwd(), item))
                browser.find_by_css('input[type="submit" i]').last.click()
                code = browser.find_by_tag('pre').last.text
                print("✅ pdf417 decoded!\n")
                print('\nCODE :\n{}'.format(code))
                log_file.write('\n\n[CODE]  {}'.format(code))
            except Exception as e:
                print("⚠️ Image Decoding Failed ⚠️\n")
                log_file.write("\n\n[DECODE_QR_ERROR]  {}".format(e))
                try:
                    key = input('\n🔑 Enter KEY manually 🔑 : ')
                except Exception as e:
                    print("[KEY_ENTER_ERROR]  {}\n\n".format(e))


def run_bf_code():
    global code, key
    try:
        print('\n Running Brainfuck Code')
        browser.visit('https://copy.sh/brainfuck/')
        editor = browser.find_by_id('editor')
        editor.first.value = code
        browser.find_by_id('run_button').first.click()
        key = browser.find_by_id('output').first.text
    except Exception as e:
        print("⚠️ Brainfuck run Failed ⚠️\n")


def run_code():
    global key, log_file, lang, code

    try:
        post_data = {
            'client_secret': CLIENT_SECRET,
            'source': code,
            'lang': lang,
            'time_limit': 7,
            'memory_limit': 262144,
        }

        resp = requests.post(
            'https://api.hackerearth.com/v3/code/run/', post_data)
        log_file.write('\n\n[POST_CODE_RESPONSE]  {}'.format(resp.json()))
        print('\n/POST_CODE_RESPONSE : {}'.format(resp.json()))
        if 'output' in resp.json():
            key = resp.json()['run_status']['output'][:-1]
        else:
            raise Exception('⚠️ Code Run Failed ⚠️\n')

    except Exception as e:
        print("⚠️ Request Exception ⚠️\n")
        log_file.write("\n\n[CODE_RUN_ERROR]  {}".format(e))

        # lang_input = input(
        #     "\n Enter Language manually [N = Not available]: ")
        # lang_input = lang_input.lower()
        lang_input = 'bf'
        if lang_input == 'n':
            key = input('\n🔑 Enter KEY manually 🔑 : ')
        elif lang_input == 'bf':
            run_bf_code()
        else:
            lang = lang_input
            run_code()


def exit_handler():
    print('⛔️ TERMINATING PROGRAM ⛔️')
    log_file.close()
    browser.quit()


atexit.register(exit_handler)

if __name__ == '__main__':
    print(sys.argv)
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()

# path= /home/arion/CODE/CTF/coding_300/fwqmBkDB/8RXzm7LJ/az34jLQM/NTFxXpQL/rbeXeWmK/u8bLWD3U/xUylmEyq/VC7OkDeX/6uPCPFXx/sTLpsHNh/i0RT09xr/hT5fE5Zv/1qoMHkBR/TZ9Xekqv/8ixPFAym/j9Wa4dLq/6Ur84YUc/5wvbqeZm/AWzVmMHS/iHjJJiF5/XzlvdfQ2/UIiXm5gC/9IfBcUQB/rwevoOmG/eRdQQufI/BceZCA3Z/pdeExhWK/6nXcYDRE/yooSZX9M/DeopFsM4/9rhbw2IR/GBhyspFx/OGuYrEPu/6avb1z1A/ZaBAL3qt/25Bv0Drv/YEV25sl9/I71jq9wC/gwQtfeZj/dBkLT6Hh/CPhIgWGb/FubF8uIp/7DvSM9is/9faqBPLm/3I26xMYo/h5lwEO29/VtVcIPMt/vGoBqxfO/l5mGHBwM/QOaqq6sB/L44zBQrx/ExbJilIY/482hW2ly/J3y2b1Mx/fOfMYa4Y/H3DsAliP
# Run SHELL script
